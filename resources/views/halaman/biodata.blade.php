<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <form action="/SEND" method="POST">
        @csrf
        <label>FirstName</label> <br>
        <input type="text" name="FirstName"> <br><br>

        <label>LastName</label> <br>
        <input type="text" name="LastName"> <br><br>

        <label>Gender:</label> <br>
        <label><input type="radio" name="Gender" value="Male"/>Male</label><br>
        <label><input type="radio" name="Gender" value="Female"/>Female</label><br>
        
        <label>Nationality:</label> <br>
        <select name="Nationality">
        <option value="indonesia">Indonesia</option> <br>
        <option value="inggris">Inggris</option> <br>
        <option value="singapura">Singapura</option> <br>
        <option value="japan">Japan</option> <br>
        <option value="south korea">South Korea</option> <br>
        </select>

        <label for="Language">Language Spoken:</label><br><br>

        <select name="Language" id="Language"></select>
        <input type="checkbox"name="Language" value="Bahasa Indonesia">
        <label for="Language"> Bahasa Indonesia</label><br>
        <input type="checkbox"name="Language" value="English">
        <label for="Language"> English</label><br>
        <input type="checkbox"name="Language" value="Other">
        <label for="Language"> Other</label><br>
        </select>

    
        <label>Bio:</label> <br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br><br>

        <input type="submit" value="SEND">
</body>
</html>